HW1

This is HW1 Single View App created by Chloe Kang (NetID: yk234).
The Data Model has been imported, and initial two people information including Dr.Telford and myself is embedded.
For functions, there are two functions, Add/Update and Find.
If the current target person's information is in Data Model, then it would find the information and display the output text.
If the current target person's netID does not matched with the information in Data Model, then it would add the information.
If the current target person's netID exist, but the information does not match with the previous one, then it would update the information under that exisiting netID.
Extra Implementation

I added Duke University logo on the top by using UIImageView.
I added role tap using UISegmentedControl instead of UITextField so that the user can pick one of three options, "Professor", "TA", "Student".

HW2

- add picker view for role
- add UIImageView for picture
- Add a field for team
- Add a field for gender with segmented control
- Putting JSON File and encode/decode
