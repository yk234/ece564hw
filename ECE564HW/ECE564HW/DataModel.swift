//
//  DataModel.swift
//  ECE564HW
//
//  Created by Chloe Kang on 9/6/22.
//
//
import Foundation
import UIKit

enum DukeRole: String {
    case Professor = "Professor"
    case TA = "TA"
    case Student = "Student"
    case Other = "Other"
}

enum Gender: String {
    case Male = "Male"
    case Female = "Female"
    case Other = "Other"
    case Unknown = "Unknown"
}

//protocol Multiple {
//    var hobbies: [String] { get }
//    var progLangs: [String] { get }
//}
class Person {
    var firstname: String = "First"
    var lastname: String = "Last"
    var wherefrom: String = "County"
    var gender: Gender = .Female
    var hobbies: [String] = []
    
}

class DukePerson: Person, CustomStringConvertible, Codable {
    
    var description: String {
        
        return self.buildInfo()
    }
//    static let DocumentsDiectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
//    static let ArchiveURL = DocumentsDiectory.appendingPathComponent("ECE564Cohort")
    
    var role: DukeRole = .Other
    var degree: String = "Not Applicable"
    var languages: [String] = []
    var picture: String = ""
    var team: String = "None"
    var netid: String = ""
    var email: String = ""
  
    convenience init(firstname: String, lastname: String, wherefrom: String, gender: Gender, hobbies: [String], role: String, languages: [String], netid: String, team: String, degree: String, email: String, picture: String){
        self.init()
        self.role  = .Student // check
        
        self.firstname = firstname
        self.lastname = lastname
        self.wherefrom = wherefrom
        self.gender = gender
        self.hobbies = hobbies
        self.languages = languages
        self.netid = netid
        self.team = team
        self.degree = degree
        self.email = email
        self.picture = picture
        
    }
    
    enum CodingKeys: String, CodingKey {
        case firstname
        case lastname
        case wherefrom
        case hobbies
        case languages
        case team
        case gender
        case role
        case degree
        case picture
        case netid
        case email
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(firstname, forKey: .firstname)
        try container.encode(lastname, forKey: .lastname)
        try container.encode(wherefrom, forKey: .wherefrom)
        try container.encode(hobbies, forKey: .hobbies)
        try container.encode(languages, forKey: .languages)
        try container.encode(team, forKey: .team)
        try container.encode(gender.rawValue, forKey: .gender)
        try container.encode(role.rawValue, forKey: .role)
        try container.encode(degree, forKey: .degree)
        try container.encode(picture, forKey: .picture)
        try container.encode(netid, forKey: .netid)
        try container.encode(email, forKey: .email)
    }
    
    public required convenience init(from decoder: Decoder) throws {
        
        self.init()
        let values = try decoder.container(keyedBy: CodingKeys.self)
        let firstname = try values.decode(String.self, forKey: .firstname)
        let lastname = try values.decode(String.self, forKey: .lastname)
        let wherefrom = try values.decode(String.self, forKey: .wherefrom)
        let hobbies = try values.decode(String.self, forKey: .hobbies)
        let languages = try values.decode(String.self, forKey: .languages)
        let team = try values.decode(String.self, forKey: .team)
        let gender = try values.decode(String.self, forKey: .gender)
        let role = try values.decode(String.self, forKey: .role)
        let degree = try values.decode(String.self, forKey: .degree)
        let picture = try values.decode(String.self, forKey: .picture)
        let netid = try values.decode(String.self, forKey: .netid)
        let email = try values.decode(String.self, forKey: .email)
    }
    
    // function to convert array to string
    func arrayToString(array: [String]) -> String {
        var outputString:String = ""
        
        for i in 0 ..< array.count {
            
            if i < (array.count - 1) {
                outputString += array[i] + ", "
            }
                
            else {
                outputString += array[i]
            }
        }
        return outputString
        
    }
    
    
    // function to convert images to base64 images
    func stringFromImage(_ imagePic: UIImage) -> String {
        let picImageData: Data = imagePic.jpegData(compressionQuality: 0.2)!
        // the smaller number we get, jpeg
        let picBase64 = picImageData.base64EncodedString()
        // built-in function for base64
        return picBase64
    }
    
    func buildInfo() -> String {
        
        let personInfo = "My name is \(self.firstname) \(self.lastname) and I am from \(self.wherefrom). My best programming language is \(arrayToString(array: languages)), and my favorite hobby is \(arrayToString(array: hobbies)). You can reach me at \(self.netid)@duke.edu."
        
        return personInfo
    }
}

// pre-stored data
var DukePeople: [DukePerson] = [
    
    .init(firstname: "Chloe", lastname: "Kang", wherefrom: "Chapel Hill, NC", gender: .Female, hobbies: ["Tennis", "Badminton"], role: "Student", languages: ["Python", "C", "C++"], netid: "yk234", team: "1", degree: "MS", email: "", picture: "chloe_photo.png"),
    
    .init(firstname: "Ric", lastname: "Telford", wherefrom: "Chatham County, NC", gender: .Male, hobbies: ["Hiking", "Swimming", "Biking"], role: "Professor", languages: ["Swift", "C", "C++"], netid: "rt113", team: "None", degree: "", email: "", picture: "")
]
