//
//  DB.swift
//  ECE564HW
//
//  Created by Chloe Kang on 9/5/22.
//

import Foundation

struct DB {
    var First_Name: String
    var Last_Name: String
    var From: String
    var ProgLang: String
    var Hobby: String
    var NetID: String
}
