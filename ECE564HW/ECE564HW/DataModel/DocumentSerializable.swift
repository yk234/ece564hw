//
//  DocumentSerializable.swift
//  ECE564HW
//
//  Created by Chloe Kang on 9/5/22.
//

import Foundation

protocol DocumentSerializable {
    init?(documentData: [String: Any])
}
