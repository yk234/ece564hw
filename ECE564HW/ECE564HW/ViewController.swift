//
//  ViewController.swift
//  ECE564HW
//
//  Created by Chloe Kang on 9/5/22.
//

import UIKit

class ViewController: UIViewController {
    
    var firstname: UITextField!
    var lastname: UITextField!
    var wherefrom: UITextField!
    var hobbies: UITextField!
    var languages: UITextField!
    var netid: UITextField!
    var role: UITextField!
    var team: UITextField!
    var degree: UITextField!
    var addUpdateButton: UIButton!
    var findButton: UIButton!
    var clearButton: UIButton!
    var picture: UIImageView!

    var gender: UISegmentedControl!

    var outPut: UILabel!

    // options for UISegmentedControl
    let roleItems = ["Professor", "TA", "Student", "Other"]
    let genderItems = ["Male", "Female", "Other", "Unknown"]

    var rolePickerView = UIPickerView()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        // change background color
        let view = UIView()
        view.backgroundColor = .white

        let label = UILabel()
        label.frame = CGRect(x: 75, y: 270, width:200, height: 20)
        label.text = ""
        label.textColor = .black
        view.addSubview(label)
        self.view = view
        
        //let data = DataLoader().PeopleInfo

//        rolePickerView.delegate = self
//        rolePickerView.dataSource = self
//        role.inputView = rolePickerView



        firstname = UITextField()
        firstname.borderStyle = .roundedRect
        firstname.frame = CGRect(x: 150, y: 100, width:200, height: 40)
        firstname.text = ""
        view.addSubview(firstname)

        let firstNameLabel = UILabel()
        firstNameLabel.frame = CGRect(x: 30, y: 110, width:100, height: 20)
        firstNameLabel.text = "First Name"
        firstNameLabel.textColor = .black
        view.addSubview(firstNameLabel)

        lastname = UITextField()
        lastname.borderStyle = .roundedRect
        lastname.frame = CGRect(x: 150, y: 150, width:200, height: 40)
        lastname.text = ""
        view.addSubview(lastname)

        let lastNameLabel = UILabel()
        lastNameLabel.frame = CGRect(x: 30, y: 160, width:100, height: 20)
        lastNameLabel.text = "Last Name"
        lastNameLabel.textColor = .black
        view.addSubview(lastNameLabel)

        gender = UISegmentedControl(items: genderItems)
        gender.addTarget(self, action: #selector(updateGender), for: .valueChanged)
        gender.translatesAutoresizingMaskIntoConstraints = false
        gender.frame = CGRect(x: 150, y: 200, width: 200, height: 40)
        view.addSubview(gender)

        let genderLabel = UILabel()
        genderLabel.frame = CGRect(x: 30, y: 210, width:100, height: 20)
        genderLabel.text = "Gender"
        genderLabel.textColor = .black
        view.addSubview(genderLabel)

        wherefrom = UITextField()
        wherefrom.borderStyle = .roundedRect
        wherefrom.frame = CGRect(x: 150, y: 250, width:200, height: 40)
        wherefrom.text = ""
        view.addSubview(wherefrom)

        let whereFromLabel = UILabel()
        whereFromLabel.frame = CGRect(x: 30, y: 260, width:100, height: 20)
        whereFromLabel.text = "From"
        whereFromLabel.textColor = .black
        view.addSubview(whereFromLabel)
        
        degree = UITextField()
        degree.borderStyle = .roundedRect
        degree.frame = CGRect(x: 150, y: 300, width:200, height: 40)
        degree.text = ""
        view.addSubview(degree)

        let degreeLabel = UILabel()
        degreeLabel.frame = CGRect(x: 30, y: 310, width:100, height: 20)
        degreeLabel.text = "Degree"
        degreeLabel.textColor = .black
        view.addSubview(degreeLabel)

        hobbies = UITextField()
        hobbies.borderStyle = .roundedRect
        hobbies.frame = CGRect(x: 150, y: 350, width:200, height: 40)
        hobbies.text = ""
        view.addSubview(hobbies)

        let hobbyLabel = UILabel()
        hobbyLabel.frame = CGRect(x: 30, y: 360, width:100, height: 20)
        hobbyLabel.text = "Hobbies"
        hobbyLabel.textColor = .black
        view.addSubview(hobbyLabel)

        languages = UITextField()
        languages.borderStyle = .roundedRect
        languages.frame = CGRect(x: 150, y: 400, width:200, height: 40)
        languages.text = ""
        view.addSubview(languages)

        let progLangLabel = UILabel()
        progLangLabel.frame = CGRect(x: 30, y: 410, width:100, height: 20)
        progLangLabel.text = "Languages"
        progLangLabel.textColor = .black
        view.addSubview(progLangLabel)

        netid = UITextField()
        netid.borderStyle = .roundedRect
        netid.frame = CGRect(x: 150, y: 450, width:200, height: 40)
        netid.text = ""
        view.addSubview(netid)

        let netIDLabel = UILabel()
        netIDLabel.frame = CGRect(x: 30, y: 460, width:100, height: 20)
        netIDLabel.text = "Email"
        netIDLabel.textColor = .black
        view.addSubview(netIDLabel)

//        role = UISegmentedControl(items: roleItems)
//        role.addTarget(self, action: #selector(updateRole), for: .valueChanged)
//        role.translatesAutoresizingMaskIntoConstraints = false
//        role.frame = CGRect(x: 150, y: 450, width: 200, height: 40)
//        view.addSubview(role)
        
        // pickerview label
        rolePickerView = UIPickerView()
        rolePickerView.delegate = self
        rolePickerView.dataSource = self
        rolePickerView.frame = CGRect(x: 150, y: 500, width: 200, height: 40)
        //rolePickerView.addTarget(self, action: #selector(updateRole), for: .valueChanged)
        //rolePickerView.center = CGPoint(x: view.bounds.midX, y: view.bounds.midY)
        view.addSubview(rolePickerView)

        let roleLabel = UILabel()
        roleLabel.frame = CGRect(x: 30, y: 510, width:100, height: 20)
        roleLabel.text = "Role"
        roleLabel.textColor = .black
        view.addSubview(roleLabel)


        let imageView = UIImageView(image: UIImage(named: "duke.png"))
        imageView.frame = CGRect(x: 30, y: 15, width: 100, height: 100)
        imageView.contentMode = .scaleAspectFit
        view.addSubview(imageView)

        team = UITextField()
        team.borderStyle = .roundedRect
        team.frame = CGRect(x: 150, y: 550, width:200, height: 40)
        team.text = ""
        view.addSubview(team)

        let teamLabel = UILabel()
        teamLabel.frame = CGRect(x: 30, y: 560, width:100, height: 20)
        teamLabel.text = "Team"
        teamLabel.textColor = .black
        view.addSubview(teamLabel)

        self.view = view


        // Add/Update button
        addUpdateButton = UIButton()
        addUpdateButton.frame = CGRect(x: 20, y: 600, width:110, height: 50)
        addUpdateButton.backgroundColor = UIColor(red: 0/255, green: 48/255, blue: 135/255, alpha: 1)
        addUpdateButton.tintColor = UIColor.lightGray
        addUpdateButton.layer.cornerRadius = 10
        addUpdateButton.isHidden = false
        addUpdateButton.titleLabel?.numberOfLines = 0
        addUpdateButton.titleLabel?.textAlignment = .center
        addUpdateButton.titleLabel?.lineBreakMode = .byWordWrapping
        addUpdateButton.setTitle("Add/Update", for: UIControl.State())
        addUpdateButton.setTitleColor(UIColor.white, for: UIControl.State())
        addUpdateButton.addTarget(self, action: #selector(addUpdateExe), for: .touchUpInside)
        view.addSubview(addUpdateButton)

        // Find button
        findButton = UIButton()
        findButton.frame = CGRect(x: 140, y: 600, width:110, height: 50)
        findButton.backgroundColor = UIColor(red: 0/255, green: 48/255, blue: 135/255, alpha: 1)
        findButton.tintColor = UIColor.lightGray
        findButton.layer.cornerRadius = 10
        findButton.isHidden = false
        findButton.titleLabel?.numberOfLines = 0
        findButton.titleLabel?.textAlignment = .center
        findButton.titleLabel?.lineBreakMode = .byWordWrapping
        findButton.setTitle("Find", for: UIControl.State())
        findButton.setTitleColor(UIColor.white, for: UIControl.State())
        findButton.addTarget(self, action: #selector(findExe), for: .touchUpInside)
        view.addSubview(findButton)
        
        // Clear button
        clearButton = UIButton()
        clearButton.frame = CGRect(x: 260, y: 600, width:110, height: 50)
        clearButton.backgroundColor = UIColor(red: 0/255, green: 48/255, blue: 135/255, alpha: 1)
        clearButton.tintColor = UIColor.lightGray
        clearButton.layer.cornerRadius = 10
        clearButton.isHidden = false
        clearButton.titleLabel?.numberOfLines = 0
        clearButton.titleLabel?.textAlignment = .center
        clearButton.titleLabel?.lineBreakMode = .byWordWrapping
        clearButton.setTitle("Clear", for: UIControl.State())
        clearButton.setTitleColor(UIColor.white, for: UIControl.State())
        clearButton.addTarget(self, action: #selector(clearExe), for: .touchUpInside)
        view.addSubview(clearButton)

        // Output message
        outPut = UILabel()
        outPut.frame = CGRect(x: 35, y: 680, width: 320, height: 150)
        outPut.numberOfLines = 7
        outPut.text = ""
        outPut.textColor = .black
        outPut.backgroundColor = .lightGray
        view.addSubview(outPut)

//        testing
//        NSLayoutConstraint.activate([
//                   role.centerXAnchor.constraint(equalTo: view.centerXAnchor),
//                   role.topAnchor.constraint(equalTo: view.topAnchor, constant: 500)
//                ])

    }

    func stringToStringArray(string: String) -> [String] {
        let array = string.components(separatedBy:",")
        return array
    }

    @objc func updateRole () {}
    @objc func updateGender () {}

//    func arrayToString(array: [String]) -> String {
//        var outputString: String = ""
//
//        for i in 0 ..< array.count {
//            if i < (array.count - 1) {
//                outputString += array[i] + ", "
//            }
//
//            else {
//                outputString += array[i]
//            }
//        }
//        return outputString
//    }

    // function to convert string to array
    func stringToArray(string: String) -> [String] {
        let array = string.components(separatedBy:",")
        return array
    }
    
    // function to convert array to string
    func arrayToString(array: [String]) -> String {
        var outputString:String = ""
        
        for i in 0 ..< array.count {
            
            if i < (array.count - 1) {
                outputString += array[i] + ", "
            }
                
            else {
                outputString += array[i]
            }
        }
        return outputString
        
    }


    // function to add and update information
    @objc func addUpdateExe(_sender:UIButton!){

        if let currentTarget = DukePeople.firstIndex(where: {$0.netid == netid.text!}) {

            DukePeople.remove(at: currentTarget)
            outPut.text = "The person has been updated"
        }
        else {
            outPut.text = "The person has been added"
        }

        var newGender: Gender

        let selectedGender = gender.titleForSegment(at: gender.selectedSegmentIndex)
        switch selectedGender {
        case "Male":
            newGender = .Male
        case "Female":
            newGender = .Female
        case "Other":
            newGender = .Other
        case "Unknown":
            newGender = .Unknown
        default:
            newGender = .Unknown
        }
        
//        var newRole: DukeRole
//
//        switch role.text {
//        case "Professor":
//            newRole = .Professor
//        case "TA":
//            newRole = .TA
//        case "Student":
//            newRole = .Student
//        case "Other":
//            newRole = .Other
//        default:
//            newRole = .Student
//        }


        let newInfo = DukePerson(firstname: firstname.text!, lastname: lastname.text!, wherefrom: wherefrom.text!, gender: newGender, hobbies: stringToArray(string: hobbies.text!), role: role.text!, languages: stringToArray(string: languages.text!), netid: netid.text!, team: team.text!, degree: degree.text!, email: netid.text! + "@duke.edu", picture: "")

        DukePeople.append(newInfo)


    }


    @objc func findExe(_ sender:UIButton!) {

        //print(DukePeople)

        if let currentTarget = DukePeople.firstIndex(where: {$0.firstname == firstname.text! && $0.lastname == lastname.text!}) {
            outPut.text = "\(DukePeople[currentTarget])"

            // decide gender
            switch DukePeople[currentTarget].gender {
            case .Male:
                gender.selectedSegmentIndex = 0

            case .Female:
                gender.selectedSegmentIndex = 1

            case .Other:
                gender.selectedSegmentIndex = 2

            case .Unknown:
                gender.selectedSegmentIndex = 3
            }
            
            firstname.text = String(DukePeople[currentTarget].firstname)
            lastname.text = String(DukePeople[currentTarget].lastname)
            wherefrom.text = String(DukePeople[currentTarget].wherefrom)
            hobbies.text = String(arrayToString(array: DukePeople[currentTarget].hobbies))
            languages.text = String(arrayToString(array: DukePeople[currentTarget].languages))
            netid.text = String(DukePeople[currentTarget].netid + "@duke.edu")
            team.text = String(DukePeople[currentTarget].team)
           // role.text = String(DukePeople[currentTarget].role) // check
            


            }

        else {
            outPut.text = "The person was not found"
        }
    }
    
    @objc func clearExe (_ sender: UIButton!) {
        firstname.text = ""
        lastname.text = ""
        wherefrom.text = ""
        hobbies.text = ""
        role.text = ""
        languages.text = ""
        netid.text = ""
        team.text = ""
        gender.selectedSegmentIndex = -1
    }



}


// Pickerview implementation for role
extension ViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    

    func numberOfComponents(in pickerView: UIPickerView) -> Int {

        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {

        return roleItems.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {

        return roleItems[row]
    }
    
//    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
//
//        role.text = roleItems[row]
//        role.resignFirstResponder()
//    }
}

