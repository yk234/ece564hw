//
//  StorageFile.swift
//  ECE564HW
//
//  Created by Chloe Kang on 9/14/22.
//

import Foundation
import UIKit

public class DataLoader: NSObject, Codable {
    
    static let DocumentsDiectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDiectory.appendingPathComponent("ECE564Cohort")
    
    @Published var PeopleInfo = [DukePerson]()
    
    override init() {
        super.init()
    }

    
    static func saveDukePeopleInfo(_ people: [DukePerson]) -> Bool {
        var outputData = Data()
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(people) {
            if let json = String(data: encoded, encoding: .utf8) {
                print(json)
                outputData = encoded
            }
            else { return false }
            
            do {
                    try outputData.write(to: ArchiveURL)
            } catch let error as NSError {
                print (error)
                return false
            }
            return true
        }
        else { return false }
    }
    
    static func loadDukePeopleInfo() -> [DukePerson]? {
        let decoder = JSONDecoder()
        var people = [DukePerson]()
        let tempData: Data
        
        do {
            tempData = try Data(contentsOf: ArchiveURL)
        } catch let error as NSError {
            print(error)
            return nil
        }
        
        if let decoded = try? decoder.decode([DukePerson].self, from: tempData) {
            print(decoded[0].firstname)
            people = decoded
        }
        
        return people
    }
}
